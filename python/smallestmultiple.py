"""
Project Euler Problem 5

2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?


Methodology: as you are iterating through each number from 1 to 20, realize that as you go higher, you can
use previous elements to help represent the number you currently are on.

Ex: 20 = 2 * 2 * 5  -  Require two 2s and one 5.
    10 = 2 * 5  -  Require one 2 and one 5.
    5 = 5  -  Require one 5.
    2 = 2  -  Require one 2.
All we care about is if the number 20 can divide into the smallest dividable number and the above demonstration
tells us exactly what we need to create a 20. As you go higher in numbers, simply keep count of the highest
amount of elements needed.
"""


__author__ = 'Ben'


if __name__ == '__main__':
    # Structure of the list: Outer list stores the references to a bunch of lists that have the following
    # structure: [[number, the number of times we multiply 'number' together to get a higher number],...]
    number_list = []

    number = 2  # Minimum value to check if a value is a multiple
    terminating_number = 20 + 1
    for number in range(number, terminating_number):
        # Used to detect if a value can be factored completely by a combination of values stored in number_list
        factored_value = float(number)

        for sub_list_index in range(len(number_list)):
            # Currently iterating through the number_list to check whether the number can be obtained through
            # the existing list of values on the list or is a exponential version of an element on the list

            # Checking if the current factored_value can be represented by a combination of existing elements.
            # If a higher degree of that element's exponent is needed, it will be updated in the list.
            current_power_needed = 0   # Tracks the amount of 'powers' needed to factor the number
            while True:
                divided_value = factored_value / number_list[sub_list_index][0]
                if divided_value.is_integer() is False:
                    # When factored_value can no longer be divided by the element in the list
                    if current_power_needed > number_list[sub_list_index][1]:
                        # Checks to see how high the exponent needs to be for the element to represent
                        # the appropriate part of the number
                        number_list[sub_list_index][1] = current_power_needed
                    break
                else:
                    # If division yields an integer value, set factored_value as the newly divided value
                    # to show that we have 'factored out an element' and also increments the current_power_needed
                    # to keep track of how many of these elements were needed to 'factor' the number completely.
                    factored_value = divided_value
                    current_power_needed += 1

        # Checking if factored_value is completely factored or is another element to some power
        if factored_value != 1.0:
            # Add element into list
            number_sub_list = [number, 1]
            number_list.append(number_sub_list)

    product = 1
    for sub_list_index in range(len(number_list)):
        product *= number_list[sub_list_index][0]**number_list[sub_list_index][1]

    print('[[base, exponent],...]: ' + str(number_list))
    print('Smallest number divisible by numbers from 1 to 20: ' + str(product))